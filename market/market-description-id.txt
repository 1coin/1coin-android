Dengan aplikasi ini Anda selalu mempunyai Onecoin dengan Anda, dalam saku Anda! Anda dapat mengirim pembayaran hanya dengan memindai kode-QR atau mendekatkan dua ponsel bersama (NFC). Onecoin Wallet dirancang untuk mudah digunakan dan diandalkan, sementara juga menjadi lebih aman dan cepat.

FITUR:

• Tidak diperlukan server awan atau layanan web! Dompet ini benar-benar peer ke peer.
• Tampilkan saldo dompet dalam Onecoin dan mata uang lainnya.
• Mengirim dan menerima Onecoin via NFC, kode-QR atau Tautan Onecoin.
• Buku alamat untuk alamat penggunaan onecoin secara reguler.
• Masukkan transaksi saat offline, akan dilaksanakan ketika online.
• Notifikasi sistem untuk konektivitas Onecoin dan menerima koin.
• Widget Apl untuk saldo Onecoin.

Anda dapat mengetahui lebih mengenai Onecoin di
http://onecoin.org

Jika Anda ingin berkontribusi kepada Onecoin Wallet, proyeknya berada di
https://github.com/Onecoin-Java/onecoin-wallet

Lisensi: GPLv3
Gunakan dengan resiko Anda!
http://www.gnu.org/licenses/gpl-3.0.en.html