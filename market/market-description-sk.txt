Onecoiny vždy so sebou vo svojom vrecku. Jednoduché a rýchle platby pomocou QR kódov. Pre obchodníkov ponúka spoľahlivé a okamžité prijímanie platieb. Onecoin Wallet je prvá mobilná Onecoin aplikácia a preukázateľne aj najbezpečnejšia.

FUNKCIE:

• Nie je potrebná žiadna registrácia ani žiadny účet.
• Zobrazenie zostatku Onecoinov v ONE alebo mONE.
• Prepočet hodnoty Onecoinov do viacerých cudzích mien.
• Posielanie a prijímanie Onecoinov pomocou NFC, QR kódov alebo Onecoin adries.
• Adresár pre často používané Onecoin adresy.
• V offline režime je platba možná aj cez Bluetooth.
• Notifikácie v stavovej lište pri prijatí Onecoinov.
• Widget s celkovým zostatkom.

Viac informácií o Onecoine
http://onecoin.org

Ak chcete prispieť do projektu Onecoin Wallet nájdete ho na adrese
https://github.com/Onecoin-Java/onecoin-wallet

Licencia: GPLv3
Použitie len na vlastné riziko!
http://www.gnu.org/licenses/gpl-3.0.en.html
