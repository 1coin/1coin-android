Porta i tuoi Onecoin sempre con te, in tasca! Paghi velocemente scansionando un QR-code. Come un commerciante, riceverai i pagamenti sicuri e istantanei. Onecoin Wallet è la prima Onecoin app mobile, e probabilmente anche la più sicura!

CARATTERISTICHE:

• Non è necessaria alcuna registrazione, servizio web o cloud! Questo portafoglio è de-centralizzato e peer to peer.
• Mostra l'ammontare di Onecoin in ONE e mONE.
• Conversione verso e dalle monete nazionali.
• Invio e ricevimento di Onecoin tramite NFC, codici QR o URL Onecoin.
• Rubrica degli indirizzi per usare indirizzi Onecoin regolarmente.
• Quando sei offline, puoi comunque ancora pagare via Bluetooth.
• Sistema di notifica per le monete ricevute.
• App Widget per il saldo Onecoin

Puoi trovare altri dettagli su Onecoin su
http://onecoin.org

Se vuoi contribuire a Onecoin Wallet, il progetto si trova su
https://github.com/Onecoin-Java/onecoin-wallet

Licenza: GPLv3
Usalo a tuo rischio!
http://www.gnu.org/licenses/gpl-3.0.en.html