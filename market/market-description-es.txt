Ten tus Onecoins siempre contigo, en tu bolsillo! Paga rápidamente escaneando un código QR. Como comerciante, recibes los pagos de forma fiable e instantáneamente. Onecoin Wallet es la primera Onecoin app para movil, e incluso la mas segura!

CARACTERÍSTICAS:

• ¡No requiere registro, servidor en la nube o servicio web! Este monedero es auténticamente entre pares (peer-to-peer).
• Muestra la cantidad de Onecoins en ONE y mONE
• Conversión a divisas nacionales.
• Envío y recepción de 1coins vía NFC, códigos QR o URL de Onecoin.
• Libreta de direcciones para guardar las direcciones Onecoin más utilizadas.
• Cuando no tengas conexión, puedes seguir pagando via Bluetooth.
• Notificación al recibir monedas.
• Widget para el saldo de 1coins.

Puedes descubrir más sobre Onecoin en
http://onecoin.org

Si quieres contribuir a Onecoin Wallet, el proyecto se encuentra en
https://github.com/Onecoin-Java/bitcoin-wallet

Licencia: GPLv3
¡Úsalo bajo tu propio riesgo!
http://www.gnu.org/licenses/gpl-3.0.en.html
